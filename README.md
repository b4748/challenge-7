# Challenge 7

Project Challenge Chapter 7 by Moch. Raditya Aulya Aramdhan
#### Applications Introduction

- This application use front-end JavaScript library name REACT
- This applications use router library name react-dom for route page application
- This application use redux library for get data from back-end API
- This application use Framework bootstrap for css Styling
- this application use react-responsive library to responsive applications front-end
- this application use styled-components library to styles application

## Run Application

This application use node JS for runtime environment

To install all package modules use :

```
& npm install

```

To run application use :

```
& npm run start

```